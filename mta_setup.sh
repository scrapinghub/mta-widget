#!/usr/bin/env bash
jupyter nbextension uninstall --py --sys-prefix mta
rm -rf mta/static/

python setup.py build
pip install -e .

jupyter nbextension install --py --symlink --sys-prefix mta
jupyter nbextension enable --py --sys-prefix mta
