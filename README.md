mta-widget
===============================

Manual Testing App

Installation
------------

To install use pip:

    $ pip install mta
    $ jupyter nbextension enable --py --sys-prefix mta


For a development installation (requires npm),

    $ git clone https://github.com/Scrapinghub/mta-widget.git
    $ cd mta-widget
    $ pip install -e .
    $ jupyter nbextension install --py --symlink --sys-prefix mta
    $ jupyter nbextension enable --py --sys-prefix mta
