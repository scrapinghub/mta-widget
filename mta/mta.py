from random import randint

import ipywidgets as widgets
import pandas as pd
from scipy.stats import hypergeom
from traitlets import Bool, Int, Set, Unicode, Dict, observe


@widgets.register
class MTA(widgets.DOMWidget):
    """An example widget."""

    _view_name = Unicode("MTAView").tag(sync=True)
    _model_name = Unicode("MTAModel").tag(sync=True)
    _view_module = Unicode("mta").tag(sync=True)
    _model_module = Unicode("mta").tag(sync=True)
    _view_module_version = Unicode("^0.0.1").tag(sync=True)
    _model_module_version = Unicode("^0.0.1").tag(sync=True)

    random_ready = Int().tag(sync=True)
    get_random = Int().tag(sync=True)

    item_number = Int().tag(sync=True)
    item_count = Int().tag(sync=True)
    item_html_table = Unicode().tag(sync=True)
    item_win_html = Unicode().tag(sync=True)
    item_url = Unicode().tag(sync=True)
    download_link = Unicode().tag(sync=True)
    item_key = Unicode().tag(sync=True)
    item_sampling_group = Unicode().tag(sync=True)
    launched = Bool().tag(sync=True)
    samples_seen = Set().tag(sync=True)
    samples_seen_n = Int().tag(sync=True)
    issues = Dict().tag(sync=True)

    def check_nested(self):
        if "." in self.url_field:
            df = pd.json_normalize(self.items[self.item_number], sep=".")
            self.item_url = df[self.url_field][0]

    def get_key(self):
        key = self.items[self.item_number]["_key"].split("/")[-1]
        return key

    def __init__(
        self,
        items,
        url_field,
        job_link,
        sample_limit,
        keys_to_sampling_groups={},
        using_conditional_sampling=False,
        use_zyteapi=False,
    ):
        super().__init__()
        self.launched = False
        self.random_ready = 0
        self.get_random = 0

        self.url_field = url_field
        self.items = items
        self.using_conditional_sampling = using_conditional_sampling
        self.keys_to_sampling_groups = keys_to_sampling_groups
        self.item_number = 0
        self.item_count = len(self.items)
        # If using cond sampling only the sample gets passed at launch, thus the limit is the total
        self.sample_limit = (
            sample_limit if not using_conditional_sampling else self.item_count
        )
        self.samples_seen = []
        self.samples_seen_n = 1  # start with 1
        self.item_url = (
            self.items[self.item_number][self.url_field]
            if self.url_field in self.items[self.item_number].keys()
            else "about:blank"
        )
        self.check_nested()

        self.item_html_table = self.get_table_dict(self.items[self.item_number])
        self.download_link_template = job_link
        self.item_key = self.get_key()
        self.use_zyteapi = use_zyteapi
        self.item_sampling_group = (
            self.keys_to_sampling_groups[self.item_key]
            if using_conditional_sampling
            else ""
        )  # start with the group of the first item
        self.download_link = job_link.format(self.item_key, self.item_key)
        self.item_win_html = self.get_item_win_html(
            self.item_html_table, self.download_link
        )

    def launch(self):
        self.launched = False
        self.launched = True

    @observe("get_random")
    def _get_random(self, change):
        if self.get_random == 1:
            temp_num = self.item_number
            self.item_number = randint(0, len(self.items))
            while temp_num == self.item_number:
                self.item_number = randint(0, len(self.items))

            item_html_table = self.get_table_dict(self.items[self.item_number])
            self.item_html_table = item_html_table
            self.item_url = (
                self.items[self.item_number][self.url_field]
                if self.url_field in self.items[self.item_number].keys()
                else "about:blank"
            )
            self.check_nested()
            self.get_random = 0

    @observe("item_number")
    def _item_number_changed(self, change):
        print("item n changes called")
        self.item_key = self.get_key()
        if self.using_conditional_sampling:
            self.item_sampling_group = self.keys_to_sampling_groups[self.item_key]
        self.download_link = self.download_link_template.format(
            self.item_key, self.item_key
        )
        item_html_table = self.get_table_dict(self.items[self.item_number])
        self.item_html_table = item_html_table
        self.item_url = (
            self.items[self.item_number][self.url_field]
            if self.url_field in self.items[self.item_number].keys()
            else "about:blank"
        )
        self.check_nested()

    def get_table_dict(self, data):
        int_html = ""
        int_html += "<table>"

        for field in data:
            int_html += "<tr>"
            int_html += f"<th>{field}</th>"

            int_html += f'<td class="{field}">'
            if isinstance(data[field], list):
                int_html += self.get_table_list(data[field])
            elif isinstance(data[field], dict):
                int_html += self.get_table_dict(data[field])
            else:
                if field == "_key":
                    last_slash_position = str(data[field]).rindex("/")
                    dash_link_to_item = (
                        f"https://app.zyte.com/p/"
                        + str(data[field])[:last_slash_position]
                        + "/item"
                        + str(data[field])[last_slash_position:]
                    )
                    int_html += f'<a class="_key" href="{dash_link_to_item}" target="_blank">{data[field]}</a>'
                else:
                    int_html += f"{data[field]}"
            int_html += '</td><td><button style="width: 30px; background-color: lightgreen;" onclick="toggle_comment_row(this)"> + </button></td>\n'
            int_html += """
                <tr class="comment-row" style="display: none"><td colspan="2" align="center">
                <table width="100%"><tbody>
                  <tr><td style="font-weight: bold">Description</td>
                      <td style="font-weight: bold">Correct value</td>
                      <td style="font-weight: bold">Screenshot link</td>
                  </tr>
                  <tr>
                    <td><textarea class="description"></textarea></td>
                    <td><textarea class="correct_value"></textarea></td>
                    <td><input class="screenshot"></td>
                    <td><button class="save_issue" onclick="save_issue(this)">Save</button></td>
                  </tr>
                </tbody></table>
                </td></tr>
            """
        int_html += "</table>"
        return int_html

    def get_table_list(self, data):
        int_html = f'<div class="field-dropdown" onclick="toggle_field_dropdown(this)"><span class="caret"></span><div class="content">{data}</div></div>'
        int_html += '<ol class="nested closed">'
        for element in data:
            int_html += "<li>"
            if isinstance(element, list):
                int_html += self.get_table_list(element)
            elif isinstance(element, dict):
                int_html += self.get_table_dict(element)
            else:
                int_html += f"{element}"
            int_html += "</li>"
        int_html += "</ol>"
        return int_html

    def get_item_win_html(self, item_html_table, download_link):
        html = (
            """
        <html><body>
        <div class="controls">
        <table class="controls-table">
            <tr>
                <td id="backward" class="controls-table-btn">◄</td>
                <td id="random" class="controls-table-btn">Random</td>
                <td id="forward" class="controls-table-btn">►</td>
                <td id="getByNumber" class="controls-table-btn">
                    <label for="item_number">Item number</label>
                    <input id="item_number" class="controls-table-btn" type="number" min="0" placeholder="Enter number" value="0">
                    <button id="getByNumberButton" class="controls-table-btn">Search</button>
                </td>
                {conditional_sampling_group_indicator}
            </tr>
            <table class="controls-table">
                <tr>
                    <td>Found: {item_count}</td>
                    <td class="controls-table-btn">
                        <a href="{download_link}" id="download_link"><b id="download">Download current item</b></a> 
                    </td>
                    <td>
                    Inspected items: <span id="inspected_items">{samples_seen}</span>/{sample_limit}
                    </td>
                    
                    
                </tr>
                
                
            </table>
            
            
        </table>
        </div>

        <div id="item_table_div" class="item">{item_html_table}</div>

        </body></html>
        <style>


        body{
            background-color: lightgray;
        }
        .controls-table-btn{
            cursor:pointer;
        }
        .controls-table{
            margin:auto;
        }
        .controls{
            width:100%;
            position: fixed;
        }
        .controls-table td{
            font-size: medium;
        }
        .controls td{
            background-color: white;
        }
        .wrapme{
            word-break: break-all;
        }
        .item{
            padding-top:100px;
        }
        .item-table th, td{
            padding: 10px;
            font-size: small;
        }
        .table, tr:nth-child(even){
            background-color: #f2f2f2;
        }
        .table, tr:nth-child(odd){
            background-color: white;
        }

        .field-dropdown{
            cursor: pointer;
        }

        .caret::before {
            float:left;
          content: "▶";
          color: black;
          display: inline-block;
          margin-right: 6px;
        }
        .caret-down::before {
            float:none;
          -ms-transform: rotate(90deg);
          -webkit-transform: rotate(90deg);
          transform: rotate(90deg);
        }
        .closed {
          display: none;
        }
        .active {
          display: block;
        }
        </style>

        <script>
        document.issues = {};
        const save_issues_event = new Event('save_issues_event');
        """
            + f"const url_field = '{self.url_field.split('.')[-1]}';"
            + """
        function toggle_field_dropdown(field_dropdown){
            field_dropdown.getElementsByClassName("caret")[0].classList.toggle("caret-down");
            field_dropdown.getElementsByClassName("content")[0].classList.toggle("closed");
            field_dropdown.nextElementSibling.classList.toggle("active");
        }

        

        function toggle_comment_row(button) {
            if (button.textContent == ' + ') {
                button.parentElement.parentElement.nextSibling.style.display = '';
                button.textContent = ' - ';
            }
            else {
                button.parentElement.parentElement.nextSibling.style.display = 'none';
                button.textContent = ' + ';
            }
        }

        function save_issue(button) {
            item_key = document.querySelector('a._key').text;
            sc_url = document.querySelector('a._key').href;
            issue_table = button.parentElement.parentElement;
            data_row = button.parentElement.parentElement.parentElement.
                parentElement.parentElement.parentElement.previousSibling;
            field = data_row.querySelector('th').textContent;
            url = document.querySelector('td.' + url_field).textContent;
            wrong_value = data_row.querySelector('td').textContent;
            description = issue_table.querySelector('textarea.description').value;
            correct_value = issue_table.querySelector('textarea.correct_value').value;
            screenshot = issue_table.querySelector('input.screenshot').value;
            if (document.issues[item_key] == null) {
                document.issues[item_key] = {};
            };
            document.issues[item_key][field] = {
                "description": description,
                "field": field,
                "sc_url": sc_url,
                "url": url,
                "wrong_value": wrong_value,
                "correct_value": correct_value,
                "screenshot": screenshot
            }
            document.dispatchEvent(save_issues_event);
            button.style.backgroundColor = 'green';
            button.textContent = 'Saved!';
        }
        </script>
        """
        )

        html = html.replace("{item_html_table}", item_html_table)
        html = html.replace("{download_link}", download_link)
        html = html.replace("{item_count}", str(self.item_count))
        html = html.replace(
            "{samples_seen}",
            str(self.samples_seen_n),
        )
        html = html.replace(
            "{sample_limit}",
            str(self.sample_limit),
        )

        if self.using_conditional_sampling:
            conditional_sampling_group_indicator = """<td class="sampling-group-div" style="background-color: rgb(255, 194, 61);">Sampling Group: <span id="sampling_group">{item_sampling_group}</span></td>"""
            html = html.replace(
                "{conditional_sampling_group_indicator}",
                conditional_sampling_group_indicator,
            )
            html = html.replace("{item_sampling_group}", self.item_sampling_group)
        else:
            html = html.replace("{conditional_sampling_group_indicator}", """<div id="sampling_group"></div>""")

        return html
