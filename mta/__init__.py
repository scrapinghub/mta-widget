from ._version import version_info, __version__

from .mta import *


def _jupyter_nbextension_paths():
    return [
        {
            "section": "notebook",
            "src": "static",
            "dest": "mta",
            "require": "mta/extension",
        }
    ]
