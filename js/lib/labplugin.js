var mta = require('./index');
var base = require('@jupyter-widgets/base');

module.exports = {
  id: 'mta',
  requires: [base.IJupyterWidgetRegistry],
  activate: function(app, widgets) {
      widgets.registerWidget({
          name: 'mta',
          version: mta.version,
          exports: mta
      });
  },
  autoStart: true
};

