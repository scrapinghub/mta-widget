var widgets = require('@jupyter-widgets/base');
var _ = require('lodash');
var item_win;
var url_win;
var that;

// Custom Model. Custom widgets models must at least provide default values
// for model attributes, including
//
//  - `_view_name`
//  - `_view_module`
//  - `_view_module_version`
//
//  - `_model_name`
//  - `_model_module`
//  - `_model_module_version`
//
//  when different from the base class.

// When serializing the entire widget state for embedding, only values that
// differ from the defaults will be specified.
var MTAModel = widgets.DOMWidgetModel.extend({
    defaults: _.extend(widgets.DOMWidgetModel.prototype.defaults(), {
        _model_name: 'MTAModel',
        _view_name: 'MTAView',
        _model_module: 'mta',
        _view_module: 'mta',
        _model_module_version: '0.0.1',
        _view_module_version: '0.0.1'
    })
});

// Custom View. Renders the widget model.
var MTAView = widgets.DOMWidgetView.extend({
    events: {
        "click": "onClick"
    },
    render: function () {
        that = this;
        console.log("render function called");
        this.item_win_html = this.model.get('item_win_html');
        this.item_html_table = this.model.get('item_html_table');
        this.item_url = this.model.get('item_url');
        this.item_number = this.model.get('item_number');
        this.item_count = this.model.get('item_count');
        this.sample_limit = this.model.get('sample_limit');
        this.sample_urls = new Set();
        this.samples_seen_n = this.model.get('samples_seen_n');
        this.item_sampling_group = this.model.get('item_sampling_group');
        // Python -> Javascript
        this.model.on('change:item_html_table', this.item_html_table_changed, this);
        this.model.on('change:item_url', this.item_url_changed, this);
        this.model.on('change:launched', this.launch, this);
    },
    item_html_table_changed: function () {
        if (this.model.get('launched') == true) {
            console.log('item_html_table_changed');
            this.item_html_table = this.model.get('item_html_table');
            this.item_number = this.model.get('item_number');
            this.samples_seen_n = this.model.get('samples_seen_n');
            this.item_sampling_group = this.model.get('item_sampling_group');

            item_table_div = item_win.document.getElementById('item_table_div');
            item_table_div.innerHTML = this.item_html_table;

            item_table_div = item_win.document.getElementById('item_number');
            item_table_div.value = this.item_number;

            sampling_group_element = item_win.document.getElementById("sampling_group");
            sampling_group_element.innerHTML = this.item_sampling_group;

            this.sample_urls.add(this.item_number);
            this.samples_seen_n = this.sample_urls.size + 1;
            this.model.set('samples_seen_n', this.samples_seen_n);

            inspected_items_div = item_win.document.getElementById('inspected_items');
            inspected_items_div.innerHTML = this.samples_seen_n;
        }
    },
    item_url_changed: async function () {
        if (this.model.get('launched') == true) {
            console.log('item_url_changed');
            this.item_url = this.model.get('item_url');
            await this.update_url_window(this.item_url);
        }
    },
    update_url_window: async function (item_url) {
        if (this.model.get('launched') == true) {
            if (this.model.get('use_zyteapi') == true) {
                let responseData = await this.get_response_with_proxy(item_url);
                url_win.document.write(responseData);
            }
            else {
                url_win.location.href = this.item_url;
            }
        }
    },
    get_response_with_proxy: function (item_url) {
        const axios = require('axios');
        return axios
            .get(item_url, {
                proxy: {
                    protocol: 'http',
                    host: 'api.zyte.com',
                    port: 8011,
                    auth: {
                        username: 'cb529a8908cd4e98860cad89378d8955',
                        password: ''
                    }
                }
            })
            .then((response) => {
                return response.data;
            })
            .catch(error => {
                console.error(error);
                return '';
            });
    },
    launch: async function () {
        if (this.model.get('launched') == true) {
            console.log('MTA was launched');

            url_win = window.open("", "", "width=100, height=100");
            url_win.resizeTo(screen.width / 2, screen.height);
            url_win.moveTo(screen.width / 2, 0);

            if (this.model.get('use_zyteapi') == true) {
                let response = await this.get_response_with_proxy(this.item_url);
                url_win.document.write(response);
            } else {
                url_win.document.location.href = this.item_url;
            };

            item_win = window.open('', "", "width=100, height=100");
            item_win.document.open();
            console.log(this.item_win_html);
            item_win.document.write(this.item_win_html);
            item_win.document.close();
            item_table_div = item_win.document.getElementById('item_number');
            item_table_div.innerHTML = this.item_number;

            item_win.resizeTo(screen.width / 2, screen.height);

            item_win.document.addEventListener(
                'save_issues_event',
                function (e) {
                    that.model.set('issues', item_win.document.issues);
                    that.model.sync('update', that.model);
                },
                false
            )

            var forward = item_win.document.getElementById('forward');
            forward.addEventListener('click', function () {
                console.log('forward');
                if (that.item_number != that.item_count) {
                    that.item_number++;
                    that.model.set('item_number', that.item_number);
                    that.model.save_changes();
                }
            });
            var backward = item_win.document.getElementById('backward');
            backward.addEventListener('click', function () {
                console.log('backward')
                if (that.item_number != 0) {
                    that.item_number--;
                    that.model.set('item_number', that.item_number);
                    that.model.save_changes();
                }
            });
            var getByNumberBtn = item_win.document.getElementById('getByNumberButton');
            getByNumberBtn.addEventListener('click', function () {
                var numberInput = item_win.document.getElementById('item_number');
                var number = parseInt(numberInput.value);
                if (!isNaN(number) && number > 0 && number < that.item_count) {
                    console.log('Search clicked with number:', number);
                    that.item_number = number;
                    that.model.set('item_number', that.item_number);
                    that.model.save_changes();
                } else {
                    console.log('Please enter an integer greater than 0 and within the item range');
                }
            });
            var random = item_win.document.getElementById('random');
            random.addEventListener('click', function () {
                console.log('ran clicked');
                that.model.set('get_random', 1);
                that.model.save_changes();
            });

        }
    }
});


module.exports = {
    MTAModel: MTAModel,
    MTAView: MTAView
};